import React from 'react';

import axios from 'axios';

export default class PersonList extends React.Component {
  state = {
    jokes: []
  }

  componentDidMount() {
    axios.get(`https://official-joke-api.appspot.com/random_ten`)
      .then(res => {
        const jokes = res.data;
        this.setState({ jokes });
      })
  }

  render() {
    return (
      <div>
        <ul>
        { this.state.jokes.map(joke => <li>{joke.setup},{joke.punchline}</li>)}<br/>
      </ul>
      </div>
      
    )
  }
}