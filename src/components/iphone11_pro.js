import React from 'react';

class iphone11_pro extends React.Component {
    render() {
        return(
            <div data-unit-id="iphone-11-pro-spring" data-analytics-section-engagement="name:hero-iphone-11-pro-spring">
				<div class="module-content">
								
					<div class="unit-wrapper theme-dark background">
						<a class="unit-link" href="/iphone-11-pro/" target="_self" rel="follow" data-analytics-region="learn more" aria-hidden="true" tabindex="-1">&nbsp;</a>
						<div class="unit-copy-wrapper">
										
							<h2 class="headline">iPhone 11 Pro</h2>
							<h3 class="subhead" role="presentation">Pro cameras. Pro&nbsp;display. Pro&nbsp;performance.</h3>
										
							<p class="price">
							<span aria-label="From twenty eight dollars and twenty nine cents per month or six hundred and seventy nine dollars with trade in." role="text">From <span class="nowrap">$28.29/mo.
							</span> or&nbsp;<span class="nowrap">$679
							</span>&nbsp;with&nbsp;trade‑in.</span><sup><a href="#footnote-1" class="footnote">1</a></sup>
							</p>
										
										
							<div class="cta-links">
								<a class="icon icon-after icon-chevronright" href="/iphone-11-pro/" target="_self" rel="follow" data-analytics-region="learn more" data-analytics-title="Learn more about iPhone 11 Pro" aria-label="Learn more about iPhone 11 Pro">Learn more</a>
								<a class="icon icon-after icon-chevronright" href="/us/shop/goto/buy_iphone/iphone_11_pro" target="_self" rel="follow" data-analytics-region="buy" data-analytics-title="Buy iPhone 11 Pro" aria-label="Buy iPhone 11 Pro">Buy</a>
							</div>
										
						</div>
							<div class="unit-image-wrapper">
								<figure class="unit-image unit-image-iphone-11-pro-spring-hero">
								</figure>
							</div>
					</div>
								
				</div>
			</div>
        );
    }
}

export default iphone11_pro;