import React from 'react';

class iphoneSe extends React.Component {
    render() {
        return(
            <div data-unit-id="iphone-se" data-analytics-section-engagement="name:hero-iphone-se">
				<div class="module-content">
								
					<div class="unit-wrapper theme-dark background">
						<a class="unit-link" href="/iphone-se/" target="_self" rel="follow" data-analytics-region="learn more" aria-hidden="true" tabindex="-1">&nbsp;</a>
						<div class="unit-copy-wrapper">
										
						<h2 class="headline">iPhone SE</h2>
						<h3 class="subhead" role="presentation">Lots to love. Less to spend. Starting at $399.</h3>
										
							<p class="price price-alt">
								<span aria-label="From nine dollars and fifty four cents per month or two hundred and twenty nine dollars with trade in." role="text">From <span class="nowrap">$9.54/mo.</span> or&nbsp;<span class="nowrap">$229</span>&nbsp;with&nbsp;trade‑in.</span><sup><a href="#footnote-1" class="footnote">1</a></sup>
							</p>
											
							<div class="cta-links">
								<a class="icon icon-after icon-chevronright" href="/iphone-se/" target="_self" rel="follow" data-analytics-region="learn more" data-analytics-title="Learn more about iPhone SE" aria-label="Learn more about iPhone SE">Learn more</a>
								<a class="icon icon-after icon-chevronright" href="/us/shop/goto/buy_iphone/iphone_se" target="_self" rel="follow" data-analytics-region="buy" data-analytics-title="Buy iPhone SE" aria-label="Buy iPhone SE">Buy</a>
							</div>
										
						</div>
						<div class="unit-image-wrapper">
							<figure class="unit-image unit-image-iphone-se-hero">
							</figure>
						</div>
					</div>
								
				</div>
			</div>
        );
    }
}

export default iphoneSe;