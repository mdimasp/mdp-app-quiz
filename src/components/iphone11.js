import React from 'react';

class iphone11 extends React.Component {
    render() {
        return(
            <div data-unit-id="iphone-11-spring" data-analytics-section-engagement="name:hero-iphone-11-spring">
				<div class="module-content">
								
					<div class="unit-wrapper">
						<a class="unit-link" href="/iphone-11/" target="_self" rel="follow" data-analytics-region="learn more" aria-hidden="true" tabindex="-1">&nbsp;</a>
						<div class="unit-copy-wrapper">
										
							<h2 class="headline">iPhone 11</h2>
							<h3 class="subhead" role="presentation">Just the right amount of&nbsp;everything.</h3>
										
							<p class="price">
							<span aria-label="From eighteen dollars and seventy cents per month or four hundred and forty nine dollars with trade in." role="text">From <span class="nowrap">$18.70/mo.</span> or&nbsp;<span class="nowrap">$449
</span>&nbsp;with&nbsp;trade‑in.</span><sup><a href="#footnote-1" class="footnote">1</a></sup>
							</p>
										
										
							<div class="cta-links">
								<a class="icon icon-after icon-chevronright" href="/iphone-11/" target="_self" rel="follow" data-analytics-region="learn more" data-analytics-title="Learn more about iPhone 11" aria-label="Learn more about iPhone 11">Learn more</a>
								<a class="icon icon-after icon-chevronright" href="/us/shop/goto/buy_iphone/iphone_11" target="_self" rel="follow" data-analytics-region="buy" data-analytics-title="Buy iPhone 11" aria-label="Buy iPhone 11">Buy</a>
							</div>
										
						</div>
						<div class="unit-image-wrapper">
							<figure class="unit-image unit-image-iphone-11-spring-hero">
							</figure>
						</div>
					</div>
								
				</div>
			</div>
        );
    }
}

export default iphone11;