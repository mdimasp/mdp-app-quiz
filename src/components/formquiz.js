import React, { Component } from 'react';
import axios from 'axios';

class FormQuiz extends Component {

    constructor(props) {
        super(props)

        this.onQuestion = this.onQuestion.bind(this);
        this.onAnswer = this.onAnswer.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            question: '',
            answer: ''
        }
    }

    onQuestion(e) {
        this.setState({ question: e.target.value })
    }
    onAnswer(e) {
        this.setState({ answer: e.target.value })
    }
    onSubmit(e) {
        e.preventDefault()

        const qaObject = {
            question: this.state.question,
            answer: this.state.answer
        };

        axios.post('https://yogski.live/api/quiz',qaObject)
        .then((res) => {
            console.log(res.data)
        }).catch((error) => {
            console.log(error)
        });
        this.setState({question: '', answer: ''})
    }
    // state = {
    //     name: '',
    // };

    // handleSubmit = event => {
    //     event.preventDefault();
    //     const form = {
    //         question: this.state.question,
    //         answer: this.state.answer
    //     }

    //     axios.post('https://yogski.live/api/quiz',{form})
    //     .then(res=>{
    //         console.log(res);
    //         console.log(res.data);
    //     })
    // }

    // handleChange = event => {
    //     this.setState({question:event.target.value},{answer:event.target.value});
    // }

  render() {
    return (
        <div>
            <h2>Quiz Submitting Form</h2>
            <form onSubmit={this.onSubmit}>
                <br />
                <label>
                    Question:
                    <input type="text" value={this.state.question} onChange={this.onQuestion}/>
                </label>
                <br />
                <label>
                    Answer:
                    <input type="text" value={this.state.answer} onChange={this.onAnswer}/>
                </label>
                <br />
                <input type="submit" value="Submit" />
            </form>
        </div>
    );
  }
}

export default FormQuiz;

// import React, { Component } from "react";

// class SubmitQuiz extends Component {

//     handleChange = (e) => {
//         this.setState({...this.state.request_data, [event.data.target]: event.data.value})
//        }
       
//                onSubmit = () => {
//                  console.log(this.state.request_data)   // you should be able to see your form data
//              }

//              constructor(){
//                 super();
//                 this.state = {
//                     question: null,
//                     answer: null
//                 };
        
//                 this.handleChange = this.handleChange.bind(this)
//             };

//     componentDidMount(){
//         fetch("https://yogski.live/api/quiz")
//             .then(response => response.json())
//             .then(response => this.setState({ profiles: response}))
//     }

//     submit_task(data) {
//         fetch("https://yogski.live/api/quiz",
//             {
//                 method: "POST",
//                 cache: "no-cache",
//                 headers:{
//                     "content_type": "application/json"
//                 },
//                 body: JSON.stringify(data)
//             })
//             .then(response=> response.json())

//     }

//     render() {
//         return (
//             <div>
//                 <h2>Quiz Submitting Form</h2>
//                 <form id="submit_quiz" onSubmit={this.onSubmit}>
//                     <label>
//                         Pertanyaan:
//                         <input type="text" name="question" onChange={this.handleChange}/>
//                     </label>
//                     <br/>
//                     <label>
//                         Jawaban:
//                         <input type="text" name="answer" onChange={this.handleChange}/>
//                     </label>
//                     <br/>
//                     <button type="submit">Submit</button>
//                 </form>
//                 <button onClick={this.submit_task(this.request_data)}>Submit</button>
//             </div>

//         );
//     }
// }

// export default SubmitQuiz;