import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import './App.css';
import './main.built.css';
import Iphonese from './components/iphoneSe';
import Iphone11 from './components/iphone11';
import Iphone11_pro from './components/iphone11_pro';
import PersonList from './components/content.js';
import FormQuiz from './components/formquiz.js';

function App() {
  return (
    <div className="App">
      <Iphonese />
      <Iphone11 />
      <Iphone11_pro />
      <PersonList />
    </div>
  );
}

export default class Form extends Component {
  render() {
    return (
      <Router>
        <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <ul className="navbar-nav mr-auto">
        <li><Link to={'/formquiz'} className="nav-link">Quiz Submitting Form</Link></li>
        </ul>
        </nav>
        <hr />
        <Switch>
          <Route exact path='/formquiz' component={FormQuiz} />
        </Switch>
        </div>
      </Router>
    );
  }
}

// export default App;